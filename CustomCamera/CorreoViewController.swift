//
//  CorreoViewController.swift
//  CustomCamera
//
//  Created by MacBook Pro TI on 09/08/18.
//  Copyright © 2018 Brian Advent. All rights reserved.
//

import UIKit
import Alamofire
import Social

class CorreoViewController: UIViewController {
    
    let URL_USER_REGISTER = "http://192.168.0.106/apiCabina/updateFoto.php"
    

    @IBOutlet weak var fondo6: UIImageView!
    @IBOutlet weak var lineas6: UIImageView!
    @IBOutlet weak var enviar: UIImageView!
    @IBOutlet weak var finalizar: UIImageView!
    @IBOutlet weak var textFieldCorreo: UITextField!
    @IBOutlet weak var quierocompartir: UIImageView!
    @IBOutlet weak var mySw: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mySw.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)

        enviar.image = #imageLiteral(resourceName: "envia")
        lineas6.image = #imageLiteral(resourceName: "lineas6")
        fondo6.image = #imageLiteral(resourceName: "fondo6")
        finalizar.image = #imageLiteral(resourceName: "finalizar6")
        quierocompartir.image = #imageLiteral(resourceName: "quierocompartir")
        textFieldCorreo.layer.borderWidth = 1.0
        let myColor = UIColor.white
        textFieldCorreo.layer.borderColor = myColor.cgColor
        
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(tapGesture1))
        finalizar.addGestureRecognizer(tap1)
        finalizar.isUserInteractionEnabled = true
        
    }
    
    func switchValueDidChange(_ sender: UISwitch) {
        if sender.isOn {
            Constants.redes = "si"
            print(Constants.redes)
        } else {
            Constants.redes = "no"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func comparte() {

        let encodedImageData =  Constants.strBase64
        let imageData = NSData(base64Encoded: encodedImageData)
        
        let share = [imageData, "Museo del Automovil", "http://museo.com"] as [Any]
        let activityViewController = UIActivityViewController(activityItems: share, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    func compartetw(){
        Constants.redes = "si"
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func tapGesture1() {
        print(Constants.ids)
        if isValidEmail(testStr: textFieldCorreo.text!){
            
            
            //creating parameters for the post request
            let parameters: Parameters=[
                "imagen":Constants.strBase64,
                "marco":Constants.marco_seleccionado,
                "id":Constants.ids,
                "correo":textFieldCorreo.text!,
                "redes": Constants.redes
            ]
            
            print("MARCO SELECCIONADO")
            print(Constants.redes)
            
            //Sending http post request
            Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters).responseJSON
                {
                    response in
                    //printing response
                    print(response)
                    
                    //getting the json value from the server
                    if let result = response.result.value {
                        
                        //converting it as NSDictionary
                        let jsonData = result as! NSDictionary
                    }
            }

            let photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "end") as! EndViewController
            
            
            
            DispatchQueue.main.async {
                self.present(photoVC, animated: true, completion: nil)
                
            }
        } else {
            let alert = UIAlertController(title: "Mensaje", message: "Ingresa un correo valido", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    

}
