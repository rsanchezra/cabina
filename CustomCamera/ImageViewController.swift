//
//  ImageViewController.swift
//  CustomCamera
//
//  Created by MacBook Pro TI on 07/08/18.
//  Copyright © 2018 Brian Advent. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet weak var instrucciones: UIImageView!
    
    @IBOutlet weak var lineas: UIImageView!
    @IBOutlet weak var marco: UIImageView!
    @IBOutlet weak var titulo: UIImageView!
    @IBOutlet weak var uno: UIImageView!
    @IBOutlet weak var dos: UIImageView!
    @IBOutlet weak var tres: UIImageView!
    @IBOutlet weak var siguiente: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addImageToUIImageView()
        // Do any additional setup after loading the view.
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(tapGesture1))
        siguiente.addGestureRecognizer(tap1)
        siguiente.isUserInteractionEnabled = true
    }
    
    func tapGesture1() {
        let photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "registro") as! RegistroViewController
        

        
        DispatchQueue.main.async {
            self.present(photoVC, animated: true, completion: nil)
            
        }
    }
    
    func addImageToUIImageView(){
        let fondo: UIImage = UIImage(named: "fondo")!
        instrucciones.image = fondo
        let lineasI: UIImage = UIImage(named: "lineas")!
        lineas.image = lineasI
        let marcoI: UIImage = UIImage(named: "marco_instrucciones")!
        marco.image = marcoI
        let tituloI: UIImage = UIImage(named: "Instrucciones")!
        titulo.image = tituloI
        let unoI: UIImage = UIImage(named: "1")!
        uno.image = unoI
        let dosI: UIImage = UIImage(named: "2")!
        dos.image = dosI
        let tresI: UIImage = UIImage(named: "3")!
        tres.image = tresI
        let siguienteI: UIImage = UIImage(named: "siguiente")!
        siguiente.image = siguienteI

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
