//
//  EndViewController.swift
//  CustomCamera
//
//  Created by MacBook Pro TI on 15/08/18.
//  Copyright © 2018 Brian Advent. All rights reserved.
//

import UIKit

class EndViewController: UIViewController {

    @IBOutlet weak var fondo7: UIImageView!
    @IBOutlet weak var logo7: UIImageView!
    @IBOutlet weak var gracias: UIImageView!
    @IBOutlet weak var lineas7: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fondo7.image = #imageLiteral(resourceName: "fondo7")
        logo7.image = #imageLiteral(resourceName: "logo7")
        gracias.image = #imageLiteral(resourceName: "gracias")
        lineas7.image = #imageLiteral(resourceName: "lineas7")

        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
            self.incio()
        })
        // Do any additional setup after loading the view.
    }

    func incio(){
        let photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "home") as! HomeViewController
        
        
        
        DispatchQueue.main.async {
            self.present(photoVC, animated: true, completion: nil)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
