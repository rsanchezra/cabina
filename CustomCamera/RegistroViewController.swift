//
//  RegistroViewController.swift
//  CustomCamera
//
//  Created by MacBook Pro TI on 05/08/18.
//  Copyright © 2018 Brian Advent. All rights reserved.
//

import UIKit
import Alamofire

class RegistroViewController: UIViewController {
    @IBOutlet weak var fondo3: UIImageView!
    

    let URL_USER_REGISTER = "http://192.168.0.106/apiCabina/register.php"
    var id_persona = ""
    //View variables
    @IBOutlet weak var instrucciones3: UIImageView!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var lineas3: UIImageView!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var textFieldEdad: UITextField!
    @IBOutlet weak var textFieldCiudad: UITextField!
    
    @IBOutlet weak var siguiente3: UIImageView!

    @IBOutlet weak var mapa3: UIImageView!
    //Button action

    @IBAction func buttonRegister(_ sender: UIButton) {
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        let fondo3I: UIImage = UIImage(named: "fondo3")!
        fondo3.image = fondo3I
        let instrucciones3I: UIImage = UIImage(named: "instruccion3")!
        instrucciones3.image = instrucciones3I
        let lineas3I: UIImage = UIImage(named: "lineas3")!
        lineas3.image = lineas3I
        let siguiente3I: UIImage = UIImage(named: "siguiente3")!
        siguiente3.image = siguiente3I
        let mapa3I: UIImage = UIImage(named: "mapas3")!
        mapa3.image = mapa3I
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(tapGesture1))
        siguiente3.addGestureRecognizer(tap1)
        siguiente3.isUserInteractionEnabled = true

    }
    
    private func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeLeft
    }
    
    private func shouldAutorotate() -> Bool {
        return true
    }
    
    func tapGesture1() {
        if (textFieldName.text ?? "").isEmpty || (textFieldEdad.text ?? "").isEmpty  || (textFieldCiudad.text ?? "").isEmpty {
            let alert = UIAlertController(title: "Mensaje", message: "Completa los campos por favor", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
        } else {
            //creating parameters for the post request
            let parameters: Parameters=[
                "name":textFieldName.text!,
                "edad":textFieldEdad.text!,
                "ciudad":textFieldCiudad.text!
            ]
            
            
            //Sending http post request
            Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters).responseJSON
                {
                    response in
                    //printing response
                    print(response)
                    
                    //getting the json value from the server
                    if let result = response.result.value {
                        
                        //converting it as NSDictionary
                        let jsonData = result as! NSDictionary
                        
                        //displaying the message in label
                        self.id_persona = (jsonData.value(forKey: "id") as! String?)!
                        Constants.ids = self.id_persona
                    }
            }
            
            let photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "camara") as! ViewController
            
            
            
            DispatchQueue.main.async {
                self.present(photoVC, animated: true, completion: nil)
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
