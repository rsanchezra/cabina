//
//  HomeViewController.swift
//  CustomCamera
//
//  Created by MacBook Pro TI on 15/08/18.
//  Copyright © 2018 Brian Advent. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var fondo1: UIImageView!
    @IBOutlet weak var lineas1: UIImageView!
    @IBOutlet weak var circulo: UIImageView!
    @IBOutlet weak var logo1: UIImageView!
    @IBOutlet weak var logoH: UIImageView!
    @IBOutlet weak var toca: UIImageView!
    @IBOutlet weak var logoHome: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fondo1.image = #imageLiteral(resourceName: "fondo7")
        lineas1.image = #imageLiteral(resourceName: "lineas7")
        circulo.image = #imageLiteral(resourceName: "circulo")
        logo1.image = #imageLiteral(resourceName: "logo1")
        logoHome.image = #imageLiteral(resourceName: "logo1")
        toca.image = #imageLiteral(resourceName: "toca")
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(siguiente))
        toca.addGestureRecognizer(tap1)
        toca.isUserInteractionEnabled = true
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(siguiente))
        logoHome.addGestureRecognizer(tap2)
        logoHome.isUserInteractionEnabled = true
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(siguiente))
        circulo.addGestureRecognizer(tap3)
        circulo.isUserInteractionEnabled = true
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(siguiente))
        lineas1.addGestureRecognizer(tap4)
        lineas1.isUserInteractionEnabled = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func siguiente(){
        let photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "inicio") as! ImageViewController
        
        DispatchQueue.main.async {
            self.present(photoVC, animated: true, completion: nil)
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
