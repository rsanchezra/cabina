//
//  PhotoViewController.swift
//  CustomCamera
//
//  Created by Brian Advent on 24/01/2017.
//  Copyright © 2017 Brian Advent. All rights reserved.
//

import UIKit
import ImageIO

class PhotoViewController: UIViewController {

    var takenPhoto:UIImage?
    
    @IBOutlet weak var marco_select: UIImageView!
    
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var fondo4: UIImageView!
    @IBOutlet weak var lineas4: UIImageView!
    @IBOutlet weak var marco1_m: UIImageView!
    @IBOutlet weak var marco2_m: UIImageView!
    @IBOutlet weak var marco3_m: UIImageView!
    @IBOutlet weak var marco4_m: UIImageView!
    @IBOutlet weak var marco5_m: UIImageView!
    @IBOutlet weak var siguiente5: UIImageView!
    
    @IBOutlet weak var marco_3m: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let availableImage = takenPhoto {
            
            var transform = CGAffineTransform.identity
            
            switch availableImage.imageOrientation {
            case .left, .leftMirrored:
                transform = transform.translatedBy(x: availableImage.size.width, y: 0.0)
                transform = transform.rotated(by: .pi / 2.0)
            case .right, .rightMirrored:
                transform = transform.translatedBy(x: 0.0, y: 0.0)
                transform = transform.rotated(by: .pi / 1.0)
                transform = transform.translatedBy(x: 0.0, y: 0.0)
                transform = transform.rotated(by: .pi / 1.0)
            case .down, .downMirrored:
                transform = transform.translatedBy(x: availableImage.size.width, y: availableImage.size.height)
                transform = transform.rotated(by: .pi)
            default:
                break
            }
            
            let cgImg = availableImage.cgImage
            
            if let context = CGContext(data: nil,
                                       width: 1280, height: 960,
                                       bitsPerComponent: (cgImg?.bitsPerComponent)!,
                                       bytesPerRow: 0, space: (cgImg?.colorSpace!)!,
                                       bitmapInfo: (cgImg?.bitmapInfo.rawValue)!) {
                
                context.concatenate(transform)
                
                if availableImage.imageOrientation == .left || availableImage.imageOrientation == .leftMirrored ||
                    availableImage.imageOrientation == .right || availableImage.imageOrientation == .rightMirrored {
                    context.draw(cgImg!, in: CGRect(x: 0.0, y: 0.0, width: 1280, height: 960))
                } else {
                    context.draw(cgImg!, in: CGRect(x: 0.0 , y: 0.0, width: 1280, height: 960))
                }
                
                if let contextImage = context.makeImage() {
                    ImageView.image = UIImage(cgImage: contextImage)
                    let imageData:NSData = UIImagePNGRepresentation(takenPhoto!)! as NSData
                    Constants.strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
                }
                
            }
            

            
        }
        fondo4.image = #imageLiteral(resourceName: "fondo4")
        lineas4.image = #imageLiteral(resourceName: "lineas4")
        marco1_m.image = #imageLiteral(resourceName: "marco1_m")
        marco2_m.image = #imageLiteral(resourceName: "marco2_m")
        marco3_m.image = #imageLiteral(resourceName: "marco3_m")
        marco_3m.image = #imageLiteral(resourceName: "marco3_m")
        marco4_m.image = #imageLiteral(resourceName: "marco4_m")
        marco5_m.image = #imageLiteral(resourceName: "marco5_m")
        marco_select.image = #imageLiteral(resourceName: "marco3_g")
        siguiente5.image = #imageLiteral(resourceName: "siguiente5")
        
//        UIView.animate(withDuration: 2.0, animations: {
  //          self.imageView.transform = CGAffineTransform(rotationAngle: (90.0 * .pi) / 90.0)
    //    })
        
        let tapmarco1 = UITapGestureRecognizer(target: self, action: #selector(marco_select1))
        marco1_m.addGestureRecognizer(tapmarco1)
        marco1_m.isUserInteractionEnabled = true
        
        let tapmarco2 = UITapGestureRecognizer(target: self, action: #selector(marco_select2))
        marco2_m.addGestureRecognizer(tapmarco2)
        marco2_m.isUserInteractionEnabled = true
        
        let tapmarco3 = UITapGestureRecognizer(target: self, action: #selector(marco_select3))
        marco3_m.addGestureRecognizer(tapmarco3)
        marco3_m.isUserInteractionEnabled = true
        
        let tapmarco3m = UITapGestureRecognizer(target: self, action: #selector(marco_select3))
        marco_3m.addGestureRecognizer(tapmarco3m)
        marco_3m.isUserInteractionEnabled = true
        
        let tapmarco4 = UITapGestureRecognizer(target: self, action: #selector(marco_select4))
        marco4_m.addGestureRecognizer(tapmarco4)
        marco4_m.isUserInteractionEnabled = true
        
        let tapmarco5 = UITapGestureRecognizer(target: self, action: #selector(marco_select5))
        marco5_m.addGestureRecognizer(tapmarco5)
        marco5_m.isUserInteractionEnabled = true
        
        let siguiente = UITapGestureRecognizer(target: self, action: #selector(nextS))
        siguiente5.addGestureRecognizer(siguiente)
        siguiente5.isUserInteractionEnabled = true
        
    }
    
    
    func goBack() {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func marco_select1() {
        marco_select.image = #imageLiteral(resourceName: "marco1_g")
        Constants.marco_seleccionado = "1"
        print(Constants.marco_seleccionado)
    }
    
    
    func marco_select2() {
        marco_select.image = #imageLiteral(resourceName: "marco2_g")
        Constants.marco_seleccionado = "2"
        print(Constants.marco_seleccionado)
    }
    
    
    func marco_select3() {
        marco_select.image = #imageLiteral(resourceName: "marco3_g")
        Constants.marco_seleccionado = "3"
        print(Constants.marco_seleccionado)
    }
    
    
    func marco_select4() {
        marco_select.image = #imageLiteral(resourceName: "marco4_g")
        Constants.marco_seleccionado = "4"
        print(Constants.marco_seleccionado)
    }
    
    
    func marco_select5() {
        marco_select.image = #imageLiteral(resourceName: "marco5_g")
        Constants.marco_seleccionado = "5"
        print(Constants.marco_seleccionado)
    }
    
    func nextS() {
        let photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "correo") as! CorreoViewController
        
        
        
        DispatchQueue.main.async {
            self.present(photoVC, animated: true, completion: nil)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
